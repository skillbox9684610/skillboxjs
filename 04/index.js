// Задача №1
// 1. Напишите генератор массивов длиной count со случайными числами от n до m. Учтите, что n и m могут быть отрицательными, а также может быть n > m или n < m. 
// 2. Выведите результат с помощью console.log.

console.log()
console.log('Задание №1')

let n = 100
let m = -5
let count = 50

function getRandom(n, m) {

    min = Math.min(n, m)
    max = Math.max(n, m)

    return Math.floor(Math.random() * (max - min + 1)) + min
}

function generateArr(n, m, count) {
    let arr = []

    for (count; count > 0; --count) {
        arr.push(getRandom(n, m))
    }

    return arr
}


console.log(generateArr(n, m, count))
console.log()


// Задача №2
// 1. Создайте с помощью цикла for массив упорядоченных чисел с количеством чисел, равным count
// 2. С помощью второго цикла перемешайте этот массив.
// 3. Выведите получившийся результат на экран с помощью console.log.

console.log('Задание №2')

function fisher() {
    let arr = []
    let count = 5

    for (let i = 1; i <= count; ++i) {
        arr.push(i);
    }

    let i = 0;
    while (i < count) {
        let random = getRandom(1, count - 1);
        let temp = arr[i]
        arr[i] = arr[random]
        arr[random] = temp
        i++;
    }

    return arr;

}

console.log(fisher());
console.log()


// Задача №3
// С помощью цикла найдите индекс (порядковый номер) элемента массива из предыдущего задания с числом n. 
// Если такой элемент не будет найден, то выведите соответствующее сообщение на экран.

console.log('Задание №3')

function findIndex(n) {
    let arr = fisher()
    console.log(arr);

    let i = 0;


    while (i <= arr.length) {

        if (arr[i]) {
            if (arr[i] != n) {
                i++;
                continue;
            } else {
                console.log(`Индекс элемента ${n} = ${i}`);
                break;
            }

        } else {
            console.log(`Такого элемента нет в массиве`);
            break
        }



    }

}

findIndex(2)
console.log()



// Задача №4
// Напишите программу, которая будет объединять два массива: arr1 и arr2.

console.log('Задание №4')

const arr1 = [2, 2, 17, 21, 45, 12, 54, 31, 53]
const arr2 = [12, 44, 23, 5]

let arrMerge = [...arr1];


for (let i = 0; i < arr2.length; i++) {
    arrMerge.push(arr2[i]);
}

console.log(arrMerge)


