// Задача №1
// Создайте функцию с названием getAge(), которая будет рассчитывать возраст по году рождения. У функции будет всего один аргумент (параметр), который нужно передать в функцию. 
// Функция должна сделать расчёт возраста по текущему году. После расчёта функция должна вернуть результат с помощью команды return.

console.log()
console.log('Задание №1')

function getAge(yearOfBirhday) {
    let age = new Date().getFullYear() - yearOfBirhday;
    return age;
}

console.log('Возраст:', getAge(2000))
console.log()


// Задача №2
// Напишите функцию filter(), которая создаёт массив email-адресов, не попавших в чёрный список. 
// В качестве аргументов функция должна принимать два массива: 
// массив строк с исходными email-адресами и массив строк с email-адресами в чёрном списке.

console.log('Задание №2')

function findIndex(n, arr) {

    let i = 0;

    while (i <= arr.length) {

        if (arr[i]) {
            if (arr[i] != n) {
                i++;
                continue;
            } else {
                return i;
            }

        } else {
            break
        }

    }
}

function filter(whiteList, blackList) {

    let clearList = [];

    for (let i = 0; i < whiteList.length; i++) {

        if (findIndex(whiteList[i], blackList) === undefined) {
            clearList.push(whiteList[i])
        }

    }

    return clearList;
}

let whiteList = ['my-email@gmail.ru', 'jsfunc@mail.ru', 'annavkmail@vk.ru', 'fullname@skill.ru', 'goodday@day.ru']
let blackList = ['jsfunc@mail.ru', 'goodday@day.ru']

console.log('Список чистых адресов: ', filter(whiteList, blackList))
console.log()


// Задача №3
// Создайте функцию arrSort(), аргументом (параметром) которой будет массив. 
// Задача функции — сделать сортировку элементов переданного массива по возрастанию. 
// Функция должна вернуть отсортированный массив, а результат выполнения функции должен быть выведен в консоль с помощью console.log.

console.log('Задание №3')


let arr1 = [2, 5, 1, 3, 4]
let arr2 = [12, 33, 3, 44, 100]
let arr3 = [0, 1]

function arrSort(arr) {
    for (let i = 0; i < arr.length - 1; i++) {
        for (let j = 0; j < arr.length; j++) {
            if (arr[j] > arr[j + 1]) {
                let temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
            }
        }
    }

    return arr;
}

console.log('Сортированный массив: ', arrSort(arr1))




