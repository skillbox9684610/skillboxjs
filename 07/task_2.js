(function () {

    function createStudentCard(student) {
        const newDiv = document.createElement('div');

        document.body.appendChild(newDiv);

        newDiv.appendChild(document.createElement('h2'))
        newDiv.appendChild(document.createElement('span'))

        document.querySelector('h2').innerHTML = student.name
        document.querySelector('span').innerHTML = `Возраст: ${student.age}`
    }

    let studentObj = {
        name: 'Alex',
        age: 24
    }

    createStudentCard(studentObj)

})();