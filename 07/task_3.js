(function () {


    function createStudentsList(listArr) {

        const ul = document.createElement('ul');

        document.body.appendChild(ul);
        
        
        for (let i = 0; i < listArr.length; i++) {

            let li = document.createElement('li')
            ul.appendChild(li)
            
            li.appendChild(document.createElement('h2'))
            li.appendChild(document.createElement('span'))

            li.querySelector('h2').innerHTML = listArr[i].name
            li.querySelector('span').innerHTML = `Возраст: ${listArr[i].age}`
        }



    }

    let allStudents = [
        { name: 'Валя', age: 11 },
        { name: 'Таня', age: 24 },
        { name: 'Рома', age: 21 },
        { name: 'Надя', age: 34 },
        { name: 'Антон', age: 7 }
    ]

    createStudentsList(allStudents)


})();