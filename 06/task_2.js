
// Задание №3
// Напишите в файле task_2.js функцию filter(), фильтрующую массив объектов по значению свойства. 
// Массив, название свойства и нужное значение должны передаваться в качестве аргументов.

console.log('\nЗадание №3')

function filter(object, prop, name) {
    let arr = []

    for (item in object) {

        if (object[item][prop] === name) {
            arr.push(object[item])
        }
    }

    return arr;
}

let objects = [
    { name: 'Василий', surname: 'Васильев' },
    { name: 'Иван', surname: 'Иванов' },
    { name: 'Иван', surname: 'Второй' },
    { name: 'Пётр', surname: 'Петров' }
]

let result = filter(objects, 'name', 'Иван')

console.log(result);
