
// Задание №1
// Создайте в файле task_1.js функцию с названием getOlderUser(), которая будет определять, кто из двух пользователей старше.
// Аргументами функции являются два пользователя в виде двух объектов. Функция должна вернуть с помощью команды return имя старшего пользователя.
// Созданную функцию нужно вызвать, передав ей два объекта: user1 и user2. Результат, который вернёт функция, необходимо вывести в консоль.

console.log('\nЗадание №1')

function getOlderUser(user1, user2) {
    if (user1.age > user2.age) {
        return user1.name
    } else {
        return user2.name
    }
}

let user1 = {
    name: 'Игорь',
    age: 17
}
let user2 = {
    name: 'Оля',
    age: 21
}

// Вызов созданной функции
let result = getOlderUser(user1, user2)

console.log(result);



// Задание №2
// Для получения большей практики вы можете попробовать определить старшего пользователя из массива пользователей.
// Напишите в файле task_1.js функцию getOlderUserArray(), в которую будете передавать массив объектов с пользователями. 
// Функция должна вернуть имя старшего пользователя.

console.log('\nЗадание №2')

function getOlderUserArray(allUsers) {
    let arr = []

    for (user in allUsers) {
        arr.push(allUsers[user].age)
    }

    maxAge = Math.max(...arr)

    for (user in allUsers) {
        if (allUsers[user].age === maxAge) {
            return allUsers[user].name
        }
    }

}

let allUsers = [
    { name: 'Валя', age: 11 },
    { name: 'Таня', age: 24 },
    { name: 'Рома', age: 21 },
    { name: 'Надя', age: 34 },
    { name: 'Антон', age: 7 }
]


console.log(getOlderUserArray(allUsers));


