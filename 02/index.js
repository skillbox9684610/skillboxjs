// Задача 1
// Вычислить площадь прямоугольника, противоположные углы которого представлены указанными точками.


let x1 = -5
let y1 = 8
let x2 = 10
let y2 = 5

function getAreaRectangle(x1, x2, y1, y2) {

    let width = Math.abs(x1 - x2)
    let height = Math.abs(y1 - y2)
    let area = width * height

    return area
}


console.log(' ')
console.log('Задание №1')
console.log('Площадь прямоугольника по заданными противоположным точкам = ', getAreaRectangle(x1, x2, y1, y2))
console.log(' ')

// Задача 2
// Вычислите дробные части чисел a и b с точностью n. Выведите получившиеся числа с помощью console.log. Выведите результаты их сравнения (>, <, ≥, ≤, ===, ≠) с помощью console.log.

let a = 13.123456789
let b = 2.123
let n = 5

let fractionalA = Math.floor((a % 1) * Math.pow(10, n))
let fractionalB = Math.floor((b % 1) * Math.pow(10, n))


console.log(' ')
console.log('Задание №2')
console.log('Дробная часть числа А = ', fractionalA)
console.log('Дробная часть числа B = ', fractionalB)
console.log('Дробная А > Дробная В', fractionalA > fractionalB)
console.log('Дробная А < Дробная В', fractionalA < fractionalB)
console.log('Дробная А >= Дробная В', fractionalA >= fractionalB)
console.log('Дробная А <= Дробная В', fractionalA <= fractionalB)
console.log('Дробная А === Дробная В', fractionalA === fractionalB)
console.log('Дробная А != Дробная В', fractionalA != fractionalB)
console.log(' ')


// Задача 3
// Вычислите дробные части чисел a и b с точностью n. Выведите получившиеся числа с помощью console.log. Выведите результаты их сравнения (>, <, ≥, ≤, ===, ≠) с помощью console.log.

let intervalStart = 100
let intervalEnd = -5


function getRandom(n, m) {

    let min = Math.min(n, m)
    let max = Math.max(n, m)

    return Math.floor(Math.random() * (max - min + 1)) + min

}


console.log(' ')
console.log('Задание №3')
console.log(`Рандомное число в диапозоне от ${intervalStart}, до ${intervalEnd} = `, getRandom(intervalStart, intervalEnd))
console.log(' ')





