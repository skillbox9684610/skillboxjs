// Задача №1
// В переменную password запишите строку с любым произвольным паролем. 
// Проверьте надёжность пароля с помощью условного оператора if. 
// Пароль является надёжным, когда в нём есть хотя бы четыре символа, один из которых — это дефис или нижнее подчёркивание. 
// Выведите в консоль сообщения «Пароль надёжный» или «Пароль недостаточно надёжный».

console.log()
console.log('Задание №1')

let password = 'ab4_'

if ((password.includes('-') || password.includes('_')) && password.length >= 4) {
    console.log(`${password} - Пароль надёжный`);
} else {
    console.log(`${password} - Пароль недостаточно надёжный`);
}

console.log()



// Задача №2

// В переменных userName, userSurname даны имя и фамилия пользователя.
// При этом в строках беспорядок с большими и маленькими буквами, и нужно оформить строки единообразно...

console.log('Задание №2')

let userName = 'alex'
let userSurname = 'amelin'

function updateRegister(text) {
    let first = text.substring(0, 1).toUpperCase()
    let last = text.substring(1).toLowerCase()

    return res = first + last
}

function checkRegister(name, surName) {

    let resName = updateRegister(name);
    let resSurName = updateRegister(surName);

    name != resName && surName != resSurName ?
        console.log('Имя и фамилия были преобразованы') : name != resName ?
            console.log('Имя было преобразовано') : surName != resSurName ?
                console.log('Фамилия была преобразована') :
                console.log('Имя и фамилия не были преобразованы');


    return resName + ' ' + resSurName;

}

console.log(checkRegister(userName, userSurname));
console.log();


// Задача №3
// В переменной number записано число. Необходимо с помощью console.log вывести сообщение, указывающее на чётность или нечётность числа.

console.log('Задание №3')

let number = 0;

if (number % 2 === 0) {
    console.log(`Число ${number} - чётное`);
} else {
    console.log(`Число ${number} - нечётное`);
}

